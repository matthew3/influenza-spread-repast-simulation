package jZombies;

import java.util.ArrayList;

import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

import com.vividsolutions.jts.geom.Coordinate;

public class BusStop {

	private ContinuousSpace<Object> space;
	private Grid<Object> grid;
	
	protected BusRoute route;
	protected Coordinate coord;
	protected ArrayList<Human> waiters;
	
	public BusStop(ContinuousSpace<Object> space, Grid<Object> grid, Coordinate coord, BusRoute route)
	{
		this.coord = coord;
		this.route = route;

		this.waiters = new ArrayList<Human>();
	}
	
	public void addWaiter(Human h)
	{
		this.waiters.add(h);
	}
	
	public void removeWaiter(Human h)
	{
		this.waiters.remove(h);
	}
	
}
