package jZombies;

import java.util.ArrayList;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

public class BusRoute {
	
	protected int stepFrequency;
	protected int stepCount;
	
	protected ContinuousSpace<Object> space;
	protected Grid<Object> grid;
	protected Context context;
	
	protected String routeName;
	protected ArrayList<BusStop> stops;
	protected ArrayList<Bus> buses;
	
	public BusRoute(String route, ArrayList<BusStop> stops, Context context, ContinuousSpace<Object> space, Grid<Object> grid)
	{
		this.stops = stops;
		this.routeName = route;
		this.space = space;
		this.grid = grid;
		this.context = context;
		
		context.add(this);
		
		this.buses = new ArrayList<Bus>();
		
		if (route.equalsIgnoreCase("Transway 1A"))
		{
			this.stepFrequency = 20;
		}
		else if (route.equalsIgnoreCase("Transway 1C"))
		{
			this.stepFrequency = 15;
		}
		else if (route.equalsIgnoreCase("Crosstown 2"))
		{
			this.stepFrequency = 15;
		}
		else if (route.equalsIgnoreCase("Central 3"))
		{
			this.stepFrequency = 22;
		}
		else if (route.equalsIgnoreCase("3 West"))
		{
			this.stepFrequency = 60;
		}
		else if (route.equalsIgnoreCase("Ottawa 4"))
		{
			this.stepFrequency = 20;
		}		
		else if (route.equalsIgnoreCase("Dominion 5"))
		{
			this.stepFrequency = 25;
		}
		else if (route.equalsIgnoreCase("Dougall 6"))
		{
			this.stepFrequency = 40;
		}
		else if (route.equalsIgnoreCase("South Windsor 7"))
		{
			this.stepFrequency = 50;
		}
		else if (route.equalsIgnoreCase("Walkerville 8"))
		{
			this.stepFrequency = 37;
		}
		else if (route.equalsIgnoreCase("Lauzon 10"))
		{
			this.stepFrequency = 35;
		}
		else if (route.equalsIgnoreCase("Parent 14"))
		{
			this.stepFrequency = 40;
		}
		else
		{
			this.stepFrequency = 20;
		}
		
		this.stepFrequency *= Params.STEPS_IN_A_MINUTE;
	}
	
	@ScheduledMethod(start=1, interval=1)
	public void step()
	{
		if (this.stepCount >= this.stepFrequency)
		{
			//add a new bus to the start of the route
			Bus b = new Bus(this, 0, 1, context, space, grid);
			this.buses.add(b);
			
			//add a new bus to the end of the route
			//b = new Bus(this, this.stops.size()-1, this.stops.size()-2, context, space, grid);
			//this.buses.add(b);
			
			//System.out.println(this.buses.size()); //checked
			
			this.stepCount = this.stepCount - this.stepFrequency;
		}
		
		this.stepCount++;
	}
}
