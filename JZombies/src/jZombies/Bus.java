package jZombies;

import java.util.ArrayList;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.grid.Grid;

import com.vividsolutions.jts.geom.Coordinate;

public class Bus {

	ContinuousSpace<Object> space;
	Grid<Object> grid;
	Context context;
	
	protected Coordinate coord;
	protected BusRoute route;
	protected ArrayList<Human> riders;
	protected int currentStop;
	protected int nextStop;
	
	public Bus(BusRoute route, int bs, int bsn, Context context, ContinuousSpace<Object> space, Grid<Object> grid)
	{
		this.context = context;
		this.space = space;
		this.grid = grid;
		
		this.route = route;
		this.currentStop = bs;
		this.nextStop = bsn;
		this.coord = this.route.stops.get(this.currentStop).coord;
		
		this.riders = new ArrayList<Human>();
		
		NdPoint busLoc = space.getLocation(this.route.stops.get(this.currentStop));
		this.coord.x = busLoc.getX();
		this.coord.y = busLoc.getY();
		
		context.add(this);
		space.getAdder().add(space, this);
		space.moveTo(this, this.coord.x, this.coord.y);
	}
	
	public void getOff(Human r)
	{
		this.riders.remove(r);
	}
	
	@ScheduledMethod(start=1, interval=1)
	public void step()
	{
		BusStop currentStop = this.route.stops.get(this.currentStop);
		
		if (this.nextStop >= this.route.stops.size() || this.nextStop < 0)
		{
			//System.out.println("Finished route");
			
			//force all humans off the bus
			for (Human h : this.riders)
			{
				h.getOffBus();
				h.targetStop = currentStop;
				System.out.println("Forcing human off bus");
			}
			
			this.route.buses.remove(this);
			this.context.remove(this);
		
			return;
		}
		
		BusStop nextStop = this.route.stops.get(this.nextStop);
	
		//if the bus has reached the next stop, pick up people, drop off people, and set the next target stop
		if (space.getDistance(space.getLocation(this), space.getLocation(nextStop)) < Params.AVG_SPEED_OF_BUS_PER_SECOND)
		{
			int oldCurrent = this.currentStop;
			this.currentStop = this.nextStop;
			
			ArrayList<Human> ridersCopy = new ArrayList<Human>(this.riders);
			
			//drop off people
			for (Human h : ridersCopy)
			{
				if (h.targetStop == nextStop)
				{
					h.getOffBus();
					this.riders.remove(h);
					System.out.println("Dropped off a human");
				}
			}
			
			//pick up people
			if (nextStop.waiters.size() > 0)
			{
				System.out.println(nextStop.waiters.size());
			}
			
			for (Human h : nextStop.waiters)
			{
				this.riders.add(h);
				h.getOnBus(this);
				System.out.println("Picked up a human");
			}
			nextStop.waiters.clear();

			this.nextStop += (this.nextStop - oldCurrent) / Math.abs(this.nextStop - oldCurrent); //move in the correct direction
		}
		else
		{	
			NdPoint myPoint = space.getLocation(this);
			NdPoint otherPoint = space.getLocation(nextStop);

			double angle = SpatialMath.calcAngleFor2DMovement(space, myPoint, otherPoint);
			space.moveByVector(this, Params.AVG_SPEED_OF_BUS_PER_SECOND, angle, 0);
			
			myPoint = space.getLocation(this);
			
			this.coord.x = myPoint.getX();
			this.coord.y = myPoint.getY();
		}
	}
}
