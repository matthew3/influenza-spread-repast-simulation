package jZombies;

import java.io.IOException;
import java.util.ArrayList;

import repast.simphony.context.Context;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.continuous.RandomCartesianAdder;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;

public class JZombiesBuilder implements ContextBuilder {

	@Override
	public Context build(Context context) {
		context.setId("JZombies");
		
		ContinuousSpaceFactory spaceFactory = 
				ContinuousSpaceFactoryFinder.createContinuousSpaceFactory(null);
		ContinuousSpace<Object> space =
				spaceFactory.createContinuousSpace("space", context,
							new RandomCartesianAdder<Object>(),
							new repast.simphony.space.continuous.InfiniteBorders<>(),
							Params.SIM_WIDTH, Params.SIM_HEIGHT);
		
		GridFactory gridFactory = GridFactoryFinder.createGridFactory(null);
		Grid<Object> grid = gridFactory.createGrid("grid", context,
								new GridBuilderParameters<Object>(new repast.simphony.space.grid.InfiniteBorders<>(),
									new SimpleGridAdder<Object>(),
									true, (int)Params.SIM_WIDTH, (int)Params.SIM_HEIGHT));
		
		NetworkBuilder<Object> netBuilder = new NetworkBuilder<Object>("infection network", context, true);
		netBuilder.buildNetwork();
		
		//create bus stops
		BusStopFactory bsf = null;
		try {
			bsf = new BusStopFactory(context, space, grid);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (bsf == null)
		{
			System.out.println("Bus stops could not be read.");
		}
		
		//create schools
		SchoolFactory sf = null;
		try {
			sf = new SchoolFactory(context, space, grid);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (sf == null)
		{
			System.out.println("Schools could not be read.");
		}
		
		Params.setParams(bsf, sf);
		
		//fix all bus stop coordinates
		bsf.fixCoordinates(context, space);
		
		//add additional info to school (closest stop)
		sf.fixCoordinates(context, space, bsf);
		
		ArrayList<School> schools = sf.getSchools();
		
		for (School s : schools)
		{
			s.closestBusStopToRoute = bsf.closestRouteStopsTo(s, bsf.getRoutes(), space);
			//System.out.println("School: " + s); //checked
		}
		
		//create the humans, assign a school/bus to each human uniformly
		int humanCount = Params.INIT_REGULAR_COUNT;
		for (int i = 0; i < humanCount; i++)
		{
			//assign random school to human
			int schoolIndex = RandomHelper.nextIntFromTo(0, schools.size()-1);
			
			School s = schools.get(schoolIndex);
			
			BusStop b = bsf.randomBusStopOnRoute(s.closestBusStopToRoute);
			
			RegularHuman rh = new RegularHuman(s, b, context, space, grid);
		}
		
		//create the infected, assign a school/bus to each human uniformly
		int infectedCount = Params.INIT_INFECTED_COUNT;
		for (int i = 0; i < infectedCount; i++)
		{
			int schoolIndex = RandomHelper.nextIntFromTo(0, schools.size()-1);
			
			School s = schools.get(schoolIndex);
			
			BusStop b = bsf.randomBusStopOnRoute(s.closestBusStopToRoute);
			
			InfectedHuman ih = new InfectedHuman(schools.get(schoolIndex), b, context, space, grid);
		}
		
		//create the immune, assign a school/bus to each human uniformly
		int immuneCount = Params.INIT_IMMUNE_COUNT;
		for (int i = 0; i < immuneCount; i++)
		{
			int schoolIndex = RandomHelper.nextIntFromTo(0, schools.size()-1);
			
			School s = schools.get(schoolIndex);
			
			BusStop b = bsf.randomBusStopOnRoute(s.closestBusStopToRoute);

			ImmuneHuman ih = new ImmuneHuman(schools.get(schoolIndex), b, context, space, grid);
		}
		
		//move each space object to the similar point on the grid domain
		for (Object obj : context)
		{
			NdPoint pt = space.getLocation(obj);
			grid.moveTo(obj, (int)pt.getX(), (int)pt.getY());
		}
		
		return context;
	}

}
