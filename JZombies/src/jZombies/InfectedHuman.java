package jZombies;

import java.util.ArrayList;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

public class InfectedHuman extends Human {

	public InfectedHuman(School s, BusStop b, Context context, ContinuousSpace<Object> space,
			Grid<Object> grid) {
		super(s, b, context, space, grid);
	}
	
	public InfectedHuman(Human h) {
		super(h);
	}

	@ScheduledMethod(start = 1, interval = 1)
	public void step()
	{
		super.step();
		
		//make decision on whether to go to school, based on stepCount
		
		//if at school, use random to calculate probabilities that other humans get infected
		if (this.state == Human.AT_SCHOOL)
		{
			ArrayList<Human> schoolListCopy = new ArrayList<Human>(this.school.attendees);
			for (Human h : schoolListCopy)
			{
				if (h.state == Human.AT_SCHOOL)
				{
					if (h instanceof RegularHuman)
					{
						double chance = RandomHelper.nextDoubleFromTo(0, 100);
						
						if (chance <= Params.CHANCE_PER_TICK_OF_CONTRACTION_SCHOOL)
						{
							//created an infected copy of the human
							InfectedHuman ih = new InfectedHuman(h);
						}
					}
				}
			}
		}
		
		//if waiting for the bus, calculate probability of infection
		else if (this.state == Human.WAIT_FOR_BUS)
		{
			ArrayList<Human> waitersListCopy = new ArrayList<Human>(this.currentStop.waiters);
			for (Human h : waitersListCopy)
			{
				if (h instanceof RegularHuman)
				{
					double rand = RandomHelper.nextDoubleFromTo(0, 100);
					if (rand < Params.CHANCE_PER_TICK_OF_CONTRACTION_BUS_STOP)
					{
						//make the human an infected one
						InfectedHuman ih = new InfectedHuman(h);
					}
				}
			}
		}
		
		//if on bus, use random to calculate probabilities that other humans get infected
		else if (this.state == Human.ON_BUS)
		{
			ArrayList<Human> ridersListCopy = new ArrayList<Human>(this.onBus.riders);
			for (Human h : ridersListCopy)
			{
				if (h instanceof RegularHuman)
				{
					double rand = RandomHelper.nextDoubleFromTo(0, 100);
					if (rand < Params.CHANCE_PER_TICK_OF_CONTRACTION_BUS)
					{
						//make the human an infected one
						InfectedHuman ih = new InfectedHuman(h);
					}
				}
			}
		}
	}

}
