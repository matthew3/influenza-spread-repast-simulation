package jZombies;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

public class RegularHuman extends Human {

	public RegularHuman(School s, BusStop b, Context context, ContinuousSpace<Object> space,
			Grid<Object> grid) {
		super(s, b, context, space, grid);
	}
	
	public RegularHuman(Human h) {
		super(h);
	}
	
	@ScheduledMethod(start = 1, interval = 1)
	public void step()
	{
		super.step();
		
		//go on bus to school (or not)
		
		//go to school (or not)
		
		//take bus home from school (or not) (only if went to school)
	}

}
