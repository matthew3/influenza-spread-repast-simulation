package jZombies;

import java.util.ArrayList;

import com.vividsolutions.jts.geom.Coordinate;

public class Params {

	public static final double CHANCE_PER_TICK_OF_CONTRACTION_SCHOOL = 0.65;
	public static final double CHANCE_PER_TICK_OF_CONTRACTION_BUS = 0.8;
	public static final double CHANCE_PER_TICK_OF_CONTRACTION_BUS_STOP = 0.5;
	
	//5 meters per second / 111 000 meters in 1 unit of UTM = ? units of longitude per second

	public static final double AVG_SPEED_OF_HUMAN_PER_SECOND = 0.0045;
	public static final double AVG_SPEED_OF_BUS_PER_SECOND = 0.052;
	public static final double STEPS_IN_A_MINUTE = 6;
	
	public static final int INIT_REGULAR_COUNT=143, INIT_INFECTED_COUNT=6, INIT_IMMUNE_COUNT=1;
	public static final double TIME_TO_GET_TO_SCHOOL = 45; //minutes
	public static double SCALE_X = 0.75, SCALE_Y = 0.75, SIM_WIDTH=25, SIM_HEIGHT=25;
	public static int SCHOOL_START_STEP, SCHOOL_END_STEP;
	public static int STEPS_IN_A_DAY = 24*60*6;
	
	public static double originX, originY, maxX, maxY;

	public static double simWidth, simHeight;
	
	public static ArrayList<School> schools;
	public static ArrayList<BusRoute> routes;
	
	public Params(double originX, double originY, double maxX, double maxY)
	{
		this.originX = originX;
		this.originY = originY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.simHeight = Params.SIM_HEIGHT;
		this.simWidth = Params.SIM_WIDTH;
	}

	public static void setParams(BusStopFactory bsf, SchoolFactory sf) {
		
		Coordinate busMax = bsf.getMax();
		Coordinate busMin = bsf.getMin();
		
		Coordinate schoolMax = sf.getMax();
		Coordinate schoolMin = sf.getMin();
		
		double originX, originY, maxX, maxY;
		
		if (busMin.x < schoolMin.x)
			originX = busMin.x;
		else
			originX = schoolMin.x;
		
		if (busMin.y < schoolMin.y)
			originY = busMin.y;
		else
			originY = schoolMin.y;
		

		if (busMax.x < schoolMax.x)
			maxX = busMax.x;
		else
			maxX = schoolMax.x;
		
		if (busMax.y < schoolMax.y)
			maxY = busMax.y;
		else
			maxY = schoolMax.y;
		 
		Params.originX = originX;
		Params.originY = originY;
		Params.maxX = maxX;
		Params.maxY = maxY;
		
		Params.schools = sf.getSchools();
		Params.routes = bsf.getRoutes();
	}
	
	public static Coordinate coordToLocalSpace(Coordinate c)
	{
		return new Coordinate(((c.x - originX) / (maxX - originX)) * Params.SIM_WIDTH,
				((c.y - originY) / (maxY - originY)) * Params.SIM_HEIGHT);
		
	}
}
