package jZombies;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

import com.vividsolutions.jts.geom.Coordinate;

public class SchoolFactory {

	private ArrayList<School> schools = new ArrayList<School>();
	private Coordinate max, min;
	
	public SchoolFactory(Context context, ContinuousSpace<Object> space, Grid<Object> grid) throws IOException
	{
		BufferedReader bf = new BufferedReader(new FileReader("data/schools.csv"));
		String line = bf.readLine();
		
		double maxx = 0.0,maxy = 0.0;
		double minx = 0.0,miny = 0.0;
		
		while (bf.ready())
		{	
			line = bf.readLine();
			
			String[] vals = line.split(",");
			int length = vals.length;
			
			Coordinate coord = new Coordinate(Double.parseDouble(vals[length-2]), Double.parseDouble(vals[length-1]));
			
			School s = new School(context, space, grid, coord);
			this.schools.add(s);
			
			//find max
			if (maxx == 0 || maxx < coord.x)
			{
				maxx = coord.x;
			}
			if (maxy == 0 || maxy < coord.y)
			{
				maxy = coord.y;
			}
			
			//find min
			if (minx == 0 || minx > coord.x)
			{
				minx = coord.x;
			}
			if (miny == 0 || miny > coord.y)
			{
				miny = coord.y;
			}
		}
		
		bf.close();
		
		this.max = new Coordinate(maxx, maxy);
		this.min = new Coordinate(minx, miny);
	}

	public ArrayList<School> getSchools() {
		return this.schools;
	}

	public Coordinate getMax() {
		return this.max;
	}

	public Coordinate getMin() {
		return this.min;
	}

	public void fixCoordinates(Context context, ContinuousSpace<Object> space, BusStopFactory bsf) {
		for (School s : this.schools)
		{
			Coordinate moveCoord = Params.coordToLocalSpace(s.coord);
			
			context.add(s);
			space.getAdder().add(space, s);
			space.moveTo(s, moveCoord.x, moveCoord.y);
			
			//find the closest bus stop to the school
			HashMap<BusRoute, BusStop> closestStops = bsf.closestRouteStopsTo(s, bsf.getRoutes(), space);
			s.closestBusStopToRoute = closestStops;
			
			s.coord = moveCoord;
		}
	}
}
