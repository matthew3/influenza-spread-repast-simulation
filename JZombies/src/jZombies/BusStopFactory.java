package jZombies;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import repast.simphony.context.Context;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

import com.vividsolutions.jts.geom.Coordinate;

public class BusStopFactory {

	private ArrayList<BusRoute> routes = new ArrayList<BusRoute>();
	private Coordinate max, min;
	
	public BusStopFactory(Context context, ContinuousSpace<Object> space, Grid<Object> grid) throws IOException
	{
		BufferedReader bf = new BufferedReader(new FileReader("data/transit.csv"));
		String line = bf.readLine();
		
		double maxx = 0.0,maxy = 0.0;
		double minx = 0.0,miny = 0.0;
		
		HashMap<String, BusRoute> busRoutes = new HashMap<String, BusRoute>();
		
		while (bf.ready())
		{	
			line = bf.readLine();
			
			String[] vals = line.split(",");
			int length = vals.length;
			
			Coordinate coord = new Coordinate(Double.parseDouble(vals[length-2]), Double.parseDouble(vals[length-1]));
			String route = vals[length-11];
			
			if (busRoutes.containsKey(route))
			{
				//route exists, add a new bus stop
				BusRoute b = busRoutes.get(route);
				b.stops.add(new BusStop(space, grid, coord, b));
			}
			else
			{
				//make a new route entry for the bus, including first bus stop
				ArrayList<BusStop> stops = new ArrayList<BusStop>();
				BusRoute br = new BusRoute(route, stops, context, space, grid);
				
				BusStop bs = new BusStop(space, grid, coord, br);
				br.stops.add(bs);
				
				busRoutes.put(route, br);
			}
			
			//find max
			if (maxx == 0 || maxx < coord.x)
			{
				maxx = coord.x;
			}
			if (maxy == 0 || maxy < coord.y)
			{
				maxy = coord.y;
			}
			
			//find min
			if (minx == 0 || minx > coord.x)
			{
				minx = coord.x;
			}
			if (miny == 0 || miny > coord.y)
			{
				miny = coord.y;
			}
		}

		this.routes = new ArrayList<BusRoute>(busRoutes.values());
		
		//remove bus routes that only have 0 or 1 bus stop
		ArrayList<BusRoute> removeRoutes = new ArrayList<BusRoute>();
		
		for (BusRoute r : this.routes)
		{
			if (r.stops.size() <= 1)
			{
				removeRoutes.add(r);
			}
		}
		
		this.routes.removeAll(removeRoutes);
		context.removeAll(removeRoutes);
		//end of removal
		
		bf.close();
		
		this.max = new Coordinate(maxx, maxy);
		this.min = new Coordinate(minx, miny);
		
	}

	public ArrayList<BusRoute> getRoutes() {
		return this.routes;
	}

	public Coordinate getMax() {
		return this.max;
	}

	public Coordinate getMin() {
		return this.min;
	}
	
	//returns map of closest routes to school
	public HashMap<BusRoute, BusStop> closestRouteStopsTo(Object s, ArrayList<BusRoute> routes, ContinuousSpace<Object> space)
	{
		HashMap<BusRoute, BusStop> result = new HashMap<BusRoute, BusStop>();
		
		for (BusRoute br : routes)
		{
			//System.out.println("route: " + br); //checked
			BusStop minB = this.closestStopTo(s, br, space);
			
			if (minB != null)
			{
				result.put(br, minB);
			}
		}
		
		return result;
	}
	
	private BusStop closestStopTo(Object h, BusRoute br, ContinuousSpace<Object> space)
	{
		BusStop closest = null;
		
		double minDist = Params.SIM_WIDTH + Params.SIM_HEIGHT;
		
		for (BusStop b : br.stops)
		{
			double dist = space.getDistance(space.getLocation(b), space.getLocation(h));
			if (dist < minDist)
			{
				closest = b;
				minDist = dist;
			}
		}
		
		if (minDist > 15)
			return null;
		
		return closest;
	}
	
	public void fixCoordinates(Context context, ContinuousSpace<Object> space)
	{
		for (BusRoute r : this.routes)
		{
			for (BusStop bs : r.stops)
			{
				Coordinate coord = Params.coordToLocalSpace(bs.coord);
				
				context.add(bs);
				space.getAdder().add(space, bs);
				space.moveTo(bs, coord.x, coord.y);
				
				bs.coord = coord;
			}
		}
	}

	public BusStop randomBusStopOnRoute(
			HashMap<BusRoute, BusStop> closestBusStopToRoute) {

		Set<BusRoute> routes = closestBusStopToRoute.keySet();
		
		int randomRouteIndex = RandomHelper.nextIntFromTo(0, routes.size()-1);
		BusRoute r = (BusRoute)routes.toArray()[randomRouteIndex];
		
		Collection<BusStop> stops = closestBusStopToRoute.values();
		
		int randomStopIndex = RandomHelper.nextIntFromTo(0, stops.size()-1);
		BusStop b = (BusStop)stops.toArray()[randomStopIndex];
		
		return b;
	}

}
