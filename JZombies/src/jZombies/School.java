package jZombies;

import java.util.ArrayList;
import java.util.HashMap;

import repast.simphony.context.Context;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

import com.vividsolutions.jts.geom.Coordinate;

public class School {

	public ContinuousSpace<Object> space;
	public Grid<Object> grid;
	public Coordinate coord;
	public HashMap<BusRoute, BusStop> closestBusStopToRoute;
	
	protected ArrayList<Human> attendees;
	public int startStep;
	public int endStep;
	
	public School(Context context, ContinuousSpace<Object> space, Grid<Object> grid, Coordinate coord)
	{
		this.space = space;
		this.grid = grid;
		this.coord = coord;
		
		this.attendees = new ArrayList<Human>();
		
		//set the starting time of school to somewhere between 8am and 8:30am
		this.startStep = RandomHelper.nextIntFromTo((int)Params.STEPS_IN_A_MINUTE * 60 * 8, (int)(Params.STEPS_IN_A_MINUTE * 60 * 8.5));
		
		//set end time to 7 hours after start => 7 hour school days 8:30am to 3:30pm
		this.endStep = this.startStep + (int)Params.STEPS_IN_A_MINUTE * 60 * 7;
	}
	
	public void addAttendee(Human h)
	{
		this.attendees.add(h);
		h.school = this;
	}
	
}
