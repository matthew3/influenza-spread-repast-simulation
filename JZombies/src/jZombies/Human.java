package jZombies;

import repast.simphony.context.Context;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.grid.Grid;

import com.vividsolutions.jts.geom.Coordinate;

public abstract class Human {

	public static int AT_SCHOOL=1, AT_HOME=2, ON_BUS=3, WAIT_FOR_BUS=4, GOING_TO_STOP=5, GOING_TO_SCHOOL=6, GOING_HOME=7;
	
	protected Coordinate coord, home;
	protected int state;
	protected int feeling; //between 1 and 100
	
	protected ContinuousSpace<Object> space;
	protected Grid<Object> grid;
	protected School school;
	protected Context context;
	protected BusStop closestBusStop;
	protected BusStop currentStop;
	protected BusStop targetStop;
	protected Bus onBus;
	
	protected int stepCount;
	
	public Human(School s, BusStop b, Context context, ContinuousSpace<Object> space, Grid<Object> grid)
	{
		this.state = AT_HOME;
		
		this.space = space;
		this.grid = grid;
		this.context = context;
		
		this.school = s;
		
		this.school.addAttendee(this);

		this.onBus = null;
		this.currentStop = null;
		this.targetStop = null;
		
		this.closestBusStop = b;
		
		//add human to space, set location
		this.coord = new Coordinate(this.closestBusStop.coord.x, this.closestBusStop.coord.y);
		this.home = new Coordinate(this.coord.x, this.coord.y);
		
		context.add(this);
		space.getAdder().add(space, this);
		space.moveTo(this, this.coord.x, this.coord.y);
	}
	
	public Human(Human h)
	{
		this.closestBusStop = h.closestBusStop;
		this.grid = h.grid;
		this.space = h.space;
		this.school = h.school;
		this.stepCount = h.stepCount;
		this.state = h.state;
		this.coord = h.coord;
		this.context = h.context;
		this.home = h.home;
		
		if (h.currentStop != null)
		{
			this.currentStop = h.currentStop;
			
			if (this.state == WAIT_FOR_BUS)
			{
				h.currentStop.waiters.remove(h);
				this.currentStop.waiters.add(this);
			}
		}
		
		if (h.targetStop != null)
		{
			this.targetStop = h.targetStop;
		}
		
		if (h.onBus != null)
		{
			this.onBus = h.onBus;
			h.onBus.riders.remove(h);
			this.onBus.riders.add(this);
		}
		
		if (h.school != null)
		{
			this.school = h.school;
			h.school.attendees.remove(h);
			this.school.attendees.add(this);
		}
		
		context.remove(h);
		context.add(this);
		
		space.getAdder().add(space, this);
		space.moveTo(this, this.coord.x, this.coord.y);
	}
	
	//@ScheduledMethod(start = 1, interval = 1), from subclass
	public void step()
	{
		//restart the human's day
		this.stepCount = (this.stepCount % Params.STEPS_IN_A_DAY) + 1;
		
		//decide whether to go to school
		if (this.state == AT_HOME)
		{	
			if (this.stepCount >= (this.school.startStep - Params.STEPS_IN_A_MINUTE*Params.TIME_TO_GET_TO_SCHOOL) - 2600)
			{
				this.currentStop = null;
				this.targetStop = this.closestBusStop;
				this.state = GOING_TO_STOP;
				
				System.out.println("going to stop");
			}
		}
		
		if (this.state == GOING_TO_STOP)
		{
			
			if (this.targetStop != null)
			{
				double dist = space.getDistance(space.getLocation(this), space.getLocation(this.targetStop));
				
				//if we're closer than 1 unit away from stop, change state
				if (dist < 1.0)
				{
					this.state = WAIT_FOR_BUS;
					this.targetStop.addWaiter(this);
					this.currentStop = this.targetStop;
					this.targetStop = null;
					
					System.out.println("waiting for the bus");
				}
				else //if we're far away from stop, move closer towards target stop
				{
					this.moveTowards(this.targetStop);
				}
			}
			else
			{
				System.out.println("No target bus stop.");
			}
		}
		
		//waiting for the bus, make the target bus stop the opposite stop to the current one
		if (this.state == WAIT_FOR_BUS)
		{	
			//if we're currently at the school, make target home
			if (this.currentStop == this.school.closestBusStopToRoute.get(this.closestBusStop.route))
			{
				this.targetStop = this.closestBusStop;
			}
			//if we're currently at home, make target school
			else if (this.currentStop == this.closestBusStop)
			{
				this.targetStop = this.school.closestBusStopToRoute.get(this.closestBusStop.route);
			}
		}
		
		//move with the bus if on the bus
		if (this.state == ON_BUS)
		{
			NdPoint busP = space.getLocation(this.onBus);
			
			/*if (busP == null || this.onBus == null)
			{
				this.state = GOING_TO_STOP;
				this.targetStop = this.currentStop;
			}*/
			
			this.coord.x = busP.getX();
			this.coord.y = busP.getY();
			
			space.moveTo(this, this.coord.x, this.coord.y);
		}
		
		//move toward the school
		if (this.state == GOING_TO_SCHOOL)
		{	
			this.moveTowards(this.school);
			
			if (this.space.getDistance(this.space.getLocation(this), this.space.getLocation(this.school)) < Params.AVG_SPEED_OF_HUMAN_PER_SECOND)
			{
				this.state = AT_SCHOOL;
				System.out.println("at school");
			}
		}
		
		//while at school
		if (this.state == AT_SCHOOL)
		{	
			if (this.stepCount >= this.school.endStep)
			{
				this.targetStop = this.school.closestBusStopToRoute.get(this.closestBusStop.route);
				this.state = GOING_TO_STOP;
				
				System.out.println("School is done, going to stop");
			}
		}
		
		if (this.state == GOING_HOME)
		{
			NdPoint p = new NdPoint(this.home.x, this.home.y);
			this.moveTowards(p);
			
			if (space.getDistance(space.getLocation(this), p) < Params.AVG_SPEED_OF_HUMAN_PER_SECOND)
			{
				this.state = AT_HOME;
			}
		}
		
	}
	
	protected void moveTowards(Object o) {
		//System.out.println("Trying to move");
		NdPoint otherPoint = space.getLocation(o);
		this.moveTowards(otherPoint);
	}
	
protected void moveTowards(NdPoint otherPoint) {
		
		//System.out.println("Trying to move");
		
		NdPoint myPoint = space.getLocation(this);

		double angle = SpatialMath.calcAngleFor2DMovement(space, myPoint, otherPoint);
		space.moveByVector(this, Params.AVG_SPEED_OF_HUMAN_PER_SECOND, angle, 0); //0.3 is speed
		
		NdPoint p = space.getLocation(this);
		
		this.coord.x = p.getX();
		this.coord.y = p.getY();
		
		//move on grid
		myPoint = space.getLocation(this);
		grid.moveTo(this , (int)myPoint.getX(), (int)myPoint.getY());
	}
	
	protected void moveTo(Coordinate c)
	{
		this.coord.x = c.x;
		this.coord.y = c.y;
		
		space.moveTo(this, c.x, c.y);
	}

	public void getOffBus() {
		if (this.targetStop == this.school.closestBusStopToRoute.get(this.closestBusStop.route))
		{
			this.state = GOING_TO_SCHOOL;
			System.out.println("going to school");
		}
		else if (this.targetStop == this.closestBusStop)
		{
			this.state = GOING_HOME;
			System.out.println("going home");
		}
		this.targetStop = null;
		this.currentStop = null;
	}

	public void getOnBus(Bus b) {
		this.state = ON_BUS;
		this.onBus = b;
		
		System.out.println("Getting on the bus");
	}
	
}