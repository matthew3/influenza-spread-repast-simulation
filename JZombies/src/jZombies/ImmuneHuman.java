package jZombies;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;

public class ImmuneHuman extends Human {

	public ImmuneHuman(School s, BusStop b, Context context, ContinuousSpace<Object> space,
			Grid<Object> grid) {
		super(s, b, context, space, grid);
	}
	
	public ImmuneHuman(Human h) {
		super(h);
	}
	
	@ScheduledMethod(start = 1, interval = 1)
	public void step()
	{
		super.step();

		//immune humans don't really affect much in the system, therefore for the moment they don't do anything
	}

}
