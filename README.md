# README #

### What is this repository for? ###

This project is for a selected topics course at the University of Windsor. The course was on data simulation, interpolation, and extrapolation. This project aims to use the City of Windsor's open data catalogue to simulate the spreading of the flu. Specifically, the system uses bus stop location and route data, as well as school location data. Each agent in the system is assigned a school and home location, and uses the closest bus route to go to school and back home. Contagious agents can pass on a positive flu status to other agents while at school, and while riding the bus. The goal is to see why spikes in flu status occur, and how quickly the virus spreads.